REFFA = "ref/Ecoli_K12_MG1655_NC_000913.fa"

SAMPLES = dict()
SAMPLES['ERR178187'] = [
    ['fastq/ERR178187_L1_R1.fastq.gz', 'fastq/ERR178187_L1_R2.fastq.gz'],
    ['fastq/ERR178187_L2_R1.fastq.gz', 'fastq/ERR178187_L2_R2.fastq.gz']]


def list_all_fqs():
    for fqlists in SAMPLES.values():
        for fqpair in fqlists:
            for fq in fqpair:
                yield fq

                
def list_all_fastqcs():
    fqcs = []
    for fq in list_all_fqs():
        fqc = fq.replace(".fastq.gz", "_fastqc.html")
        fqc = fqc.replace("fastq/", "fastqc/")
        fqcs.append(fqc)
    return fqcs
                           

rule all:
    input:
        bams = expand("bam/{sample}.merged.bam",
                      sample=SAMPLES),
        fastqcs = list_all_fastqcs()

        
rule fastqc:
    # the following only works because ordering between input and
    # output doesn't matter
    input:
        list_all_fqs()
    output:
        list_all_fastqcs()
    log:
        "fastqc/fastqc.log"
    message:
        "Running FastQC (log: {log})"
    threads:
        4
    shell:
        "fastqc -o fastqc -t {threads} {input} >& {log}"
        # note: output file cannot be given explicitely for fastqc

        
rule mapreads:
    input:
        reffa = REFFA,
        refidx = REFFA + ".pac",
        fastq = ["fastq/{sample}_L{lane}_R1.fastq.gz", "fastq/{sample}_L{lane}_R2.fastq.gz"]
    output:
        bam = temp("bam/{sample}_L{lane}.bam")
    log:
        "bam/{sample}_L{lane}.bam.log"
    message:
        "Mapping reads (log: {log})"
    threads:
        8
    shell:
        "{{ bwa mem -t {threads} {input.reffa} {input.fastq} | "
        " samtools sort -T aa -@ {threads} -o {output.bam} -; }} >& {log}"



def list_lane_bams_for_sample(wildcards):
    bams = []
    for lane in list_lanes_for_sample(wildcards.sample):
        bambase = "{}_L{}.bam".format(wildcards.sample, lane)
        bam = os.path.join(wildcards.dirname, bambase)
        bams.append(bam)
    return bams


def list_lanes_for_sample(sample):
    lanes = []
    for fqpair in SAMPLES[sample]:
        fq1 = os.path.basename(fqpair[0])
        _, lane, _  = fq1.split("_")# lazy
        assert lane[0] == "L"
        lanes.append(lane[1])
    return lanes


rule bammerge:
    input:
        bams = lambda wc: expand("{dirname}/{sample}_L{lane}.bam",
                                 dirname=wc.dirname, sample=wc.sample,
                                 lane=list_lanes_for_sample(wc.sample))
        #bams = list_lane_bams_for_sample
    output:
        bam = "{dirname}/{sample}.merged.bam"
    log:
        "{dirname}/{sample}.merged.bam.log"
    message:
        "Merging BAM files"
    threads:
        2
    shell:
        "samtools merge -@ {threads} {output.bam} {input.bams} >& {log}"

        
        

