#!/bin/bash

REF=ref/Ecoli_K12_MG1655_NC_000913.fa
REGS="NC_000913:0-1000000 NC_000913:1000000-2000000 NC_000913:2000000-3000000\
 NC_000913:3000000-4000000 NC_000913:4000000-4639675"
FQ_LANE1="fastq/ERR178187_L1_R1.fastq.gz fastq/ERR178187_L1_R2.fastq.gz"
FQ_LANE2="fastq/ERR178187_L2_R1.fastq.gz fastq/ERR178187_L2_R2.fastq.gz"

OUTDIR=out/

baml1=$OUTDIR/bam/ERR178187_L1.bam
baml2=$OUTDIR/bam/ERR178187_L2.bam
bam=$OUTDIR/bam/ERR178187.bam
finalvcf=$OUTDIR/vcf/ERR178187_lofreq.vcf.gz
for d in $OUTDIR/bam $OUTDIR/vcf $OUTDIR/fastqc; do
    test -d $d || mkdir -p $d
done

echo "INFO: Starting FastQC"
fastqc -o $OUTDIR/fastqc -t 2 $FQ_LANE1 $FQ_LANE2 >& $OUTDIR/fastqc.log
echo "INFO: FastQC complete"

echo "INFO: Starting Alignment"
{ bwa mem -t 4 $REF $FQ_LANE1 | samtools sort -@ 2 -o $baml1 -; } >& ${baml1}.log;
{ bwa mem -t 4 $REF $FQ_LANE2 | samtools sort -@ 2 -o $baml2 -; } >& ${baml2}.log;
echo "INFO: Alignment complete"

echo "INFO: Preparing BAM file"
samtools merge -@ 2 $bam $baml1 $baml2
samtools index $bam
echo "INFO: BAM complete"

echo "INFO: Calling LoFreq per region"
for r in $REGS; do
    lofreq call -B -b 1 -r $r -f $REF  $bam \
           -o $OUTDIR/vcf/region_${r}.vcf.gz >& $OUTDIR/vcf/region_${r}.vcf.gz.log;
done >& $OUTDIR/vcf/lofreq_regions.log;

bcftools merge -O z -o $finalvcf $OUTDIR/vcf/region_*vcf.gz
echo "INFO: Successful exit";




