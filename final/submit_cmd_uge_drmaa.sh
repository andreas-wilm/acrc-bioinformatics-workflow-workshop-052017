args="--timestamp
--printshellcmds
--use-conda
--configfile conf.yaml
--latency-wait 60
--max-jobs-per-second 1
--cluster-config cluster.yaml
--jobs 10
--drmaa \" -pe OpenMP {threads} -l mem_free={cluster.mem} -l h_rt={cluster.time} -cwd -e clusterlogs -o clusterlogs -w n -V
"
# -V makes sure that conda is found even if we are running from an env

echo DRMAA_LIBRARY_PATH=\$SGE_ROOT/lib/lx-amd64/libdrmaa.so snakemake $args | tr '\n' ' '
echo
