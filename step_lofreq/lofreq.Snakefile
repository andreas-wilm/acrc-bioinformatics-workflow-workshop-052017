REFFA = "ref/Ecoli_K12_MG1655_NC_000913.fa"
REF_REGIONS = ["NC_000913:0-1000000",
               "NC_000913:1000000-2000000",
               "NC_000913:2000000-3000000",
               "NC_000913:3000000-4000000",
               "NC_000913:4000000-4639675"]
SAMPLES = dict()
SAMPLES['ERR178187'] = [
    ['fastq/ERR178187_L1_R1.fastq.gz', 'fastq/ERR178187_L1_R2.fastq.gz'],
    ['fastq/ERR178187_L2_R1.fastq.gz', 'fastq/ERR178187_L2_R2.fastq.gz']]


def list_all_fqs():
    for fqlists in SAMPLES.values():
        for fqpair in fqlists:
            for fq in fqpair:
                yield fq

                
def list_all_fastqcs():
    fqcs = []
    for fq in list_all_fqs():
        fqc = fq.replace(".fastq.gz", "_fastqc.html")
        fqc = fqc.replace("fastq/", "fastqc/")
        fqcs.append(fqc)
    return fqcs
                           

def list_lanes_for_sample(sample):
    lanes = []
    for fqpair in SAMPLES[sample]:
        fq1 = os.path.basename(fqpair[0])
        _, lane, _  = fq1.split("_")# lazy
        assert lane[0] == "L"
        lanes.append(lane[1])
    return lanes


rule all:
    input:
        bams = expand("bam/{sample}.merged.bam",
                      sample=SAMPLES),
        fastqcs = list_all_fastqcs(),
        #vcfs = expand("vcf/{sample}_lofreq.vcf.gz", sample=SAMPLES)

        
rule fastqc:
    # the following only works because ordering between input and
    # output doesn't matter
    input:
        list_all_fqs()
    output:
        list_all_fastqcs()
    log:
        "fastqc/fastqc.log"
    message:
        "Running FastQC (log: {log})"
    threads:
        4
    shell:
        "fastqc -o fastqc -t {threads} {input} >& {log}"
        # note: output file cannot be given explicitely for fastqc

        
rule mapreads:
    input:
        reffa = REFFA,
        refidx = REFFA + ".pac",
        fastq = ["fastq/{sample}_L{lane}_R1.fastq.gz", "fastq/{sample}_L{lane}_R2.fastq.gz"]
    output:
        bam = temp("bam/{sample}_L{lane}.bam")
    log:
        "bam/{sample}_L{lane}.bam.log"
    message:
        "Mapping reads (log: {log})"
    threads:
        8
    shell:
        "{{ bwa mem -t {threads} {input.reffa} {input.fastq} | "
        " samtools sort -T aa -@ {threads} -o {output.bam} -; }} >& {log}"


rule bammerge:
    input:
        bams = lambda wc: expand("{dirname}/{sample}_L{lane}.bam",
                                 dirname=wc.dirname, sample=wc.sample,
                                 lane=list_lanes_for_sample(wc.sample))
    output:
        bam = "{dirname}/{sample}.merged.bam"
    log:
        "{dirname}/{sample}.merged.bam.log"
    message:
        "Merging BAM files"
    threads:
        2
    shell:
        "samtools merge -@ {threads} {output.bam} {input.bams} >& {log}"

        
rule faindex:
    input:
        "{prefix}.{suffix}"
    output:
        "{prefix}.{suffix,(fasta|fa)}.fai"
    log:
        "{prefix}.{suffix,(fasta|fa)}.fai.log"
    message:
        "Indexing {input}"
    shell:
        "samtools faidx {input} >& {log}"


# on cluster: consider adding to actual bam creating rule
rule bamindex:
    input:
        "{prefix}.bam"
    output:
        "{prefix}.bam.bai"
    log:
        "{prefix}.bam.bai.log"
    message:
        "Indexing {input}"
    threads:
        1
    shell:
        "samtools index -@ {threads} {input} >& {log}"
        

rule lofreq:
    input:
        bam = "bam/{sample}.merged.bam",
        bamindex = "bam/{sample}.merged.bam.bai",
        reffa = REFFA,
        reffaidx = REFFA + ".fai"
    output:
        temp("vcf/{sample}_lofreq.{region}.vcf.gz")
    log:
        "vcf/{sample}_lofreq.{region}.vcf.gz.log"
    message:
        "Calling variants with LoFreq on {input}"
    threads:
        1
    params:
        region = lambda wc: wc.region
    conda:
        "env-lofreq.yaml"
    shell:
        """lofreq call -B -b 1 -r {params.region} -f {input.reffa} {input.bam} -o {output}"""


rule merge_vcf:
    input:
        expand("vcf/{{sample}}_lofreq.{region}.vcf.gz", region=REF_REGIONS)
    output:
        "vcf/{sample}_lofreq.vcf.gz"
    log:
        "vcf/{sample}_lofreq.vcf.gz.log"
    message:
        "Merging VCF files"
    shell:
        "bcftools merge -O z -o {output} {input}"
    
