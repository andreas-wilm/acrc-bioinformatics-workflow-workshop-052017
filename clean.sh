for d in $(find . -name out -type d); do
    find $d -type f -exec rm {} \;
done

test -d ./final/clusterlogs && rm -rf ./final/clusterlogs

find . -name .snakemake -type d -exec rm -rf {} \;

