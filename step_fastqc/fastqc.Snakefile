REFFA = "ref/Ecoli_K12_MG1655_NC_000913.fa"

SAMPLES = dict()
SAMPLES['ERR178187'] = [
    ['fastq/ERR178187_L1_R1.fastq.gz', 'fastq/ERR178187_L1_R2.fastq.gz'],
    ['fastq/ERR178187_L2_R1.fastq.gz', 'fastq/ERR178187_L2_R2.fastq.gz']]
LANES = ["1", "2"]# shortcut!


def list_all_fqs():
    for fqlists in SAMPLES.values():
        for fqpair in fqlists:
            for fq in fqpair:
                yield fq

                
def list_all_fastqcs():
    fqcs = []
    for fq in list_all_fqs():
        fqc = fq.replace(".fastq.gz", "_fastqc.html")
        fqc = fqc.replace("fastq/", "fastqc/")
        fqcs.append(fqc)
    return fqcs

        
rule all:
    input:
        bams = expand("bam/{sample}_L{lane}.bam",
                      sample=SAMPLES,
                      lane=LANES),
        fastqcs = list_all_fastqcs()

        
rule fastqc:
    input:
        "fastq/{prefix}.fastq.gz"
        #expand("fastq/{{sample}}_L{lane}_R{readno}.fastq.gz", 
        #       lane=LANES, readno=["1", "2"])
    output:
        "fastqc/{prefix}_fastqc.html"
        #expand("fastqc/{{sample}}_L{lane}_R{readno}_fastqc.html", 
        #       lane=LANES, readno=["1", "2"])
    log:
        "fastqc/{prefix}.fastqc.log"
        #"fastqc/fastqc.log"
    message:
        "Running FastQC (log: {log})"
    threads:
        4
    shell:
        "fastqc -o fastqc -t {threads} {input} >& {log}"
        # note: output file cannot be given explicitely for fastqc

        
rule mapreads:
    input:
        reffa = REFFA,
        refidx = REFFA + ".pac",
        fastq = ["fastq/{sample}_L{lane}_R1.fastq.gz", "fastq/{sample}_L{lane}_R2.fastq.gz"]
    output:
        bam = "bam/{sample}_L{lane}.bam"
    log:
        "bam/{sample}_L{lane}.bam.log"
    message:
        "Mapping reads (log: {log})"
    threads:
        8
    shell:
        "{{ bwa mem -t {threads} {input.reffa} {input.fastq} | "
        " samtools sort -T aa -@ {threads} -o {output.bam} -; }} >& {log}"


        

